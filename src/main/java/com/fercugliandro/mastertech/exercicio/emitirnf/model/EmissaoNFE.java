package com.fercugliandro.mastertech.exercicio.emitirnf.model;

import javax.persistence.*;
import java.util.List;

@Entity(name = "tb_emissao_nfe")
public class EmissaoNFE {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String identidade;

    @Column
    private Double valor;

    @Column
    @Enumerated(EnumType.STRING)
    private StatusNFE status;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_nfe")
    private NFE nfe;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdentidade() {
        return identidade;
    }

    public void setIdentidade(String identidade) {
        this.identidade = identidade;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public StatusNFE getStatus() {
        return status;
    }

    public void setStatus(StatusNFE status) {
        this.status = status;
    }

    public NFE getNfe() {
        return nfe;
    }

    public void setNfe(NFE nfe) {
        this.nfe = nfe;
    }
}
