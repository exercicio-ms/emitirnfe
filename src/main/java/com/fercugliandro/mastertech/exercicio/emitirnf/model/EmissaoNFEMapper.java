package com.fercugliandro.mastertech.exercicio.emitirnf.model;

import com.fercugliandro.mastertech.exercicio.emitirnf.model.dto.CreateEmissaoNfeRequest;
import com.fercugliandro.mastertech.exercicio.emitirnf.model.dto.CreateEmissaoNfeResponse;
import org.springframework.stereotype.Component;

@Component
public class EmissaoNFEMapper {

    public EmissaoNFE toEmissaoNFE(CreateEmissaoNfeRequest request) {

        EmissaoNFE emissao = new EmissaoNFE();
        emissao.setIdentidade(request.getIdentidade());
        emissao.setValor(request.getValor());
        emissao.setStatus(StatusNFE.PENDING);
        emissao.setNfe(null);

        return emissao;
    }

    public CreateEmissaoNfeResponse toCreateEmissaoNFResponse(EmissaoNFE emissao) {
        CreateEmissaoNfeResponse response = new CreateEmissaoNfeResponse();
        response.setStatus(emissao.getStatus().getStatus());

        return response;
    }

}
