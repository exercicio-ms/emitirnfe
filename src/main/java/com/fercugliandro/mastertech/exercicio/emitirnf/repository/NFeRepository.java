package com.fercugliandro.mastertech.exercicio.emitirnf.repository;

import com.fercugliandro.mastertech.exercicio.emitirnf.model.NFE;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NFeRepository extends JpaRepository<NFE, Long> {
}
