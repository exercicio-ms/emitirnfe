package com.fercugliandro.mastertech.exercicio.emitirnf.model;

import com.fercugliandro.mastertech.exercicio.emitirnf.model.dto.CreateEmissaoNfeRequest;
import com.fercugliandro.mastertech.exercicio.emitirnf.model.dto.CreateEmissaoNfeResponse;
import com.fercugliandro.mastertech.exercicio.emitirnf.model.dto.CreateNFERequest;
import org.springframework.stereotype.Component;

@Component
public class NFEMapper {

   public NFE toNFE(CreateNFERequest request) {
       NFE nfe = new NFE();
       nfe.setValorInicial(request.getValorInicial());
       nfe.setValorCofins(request.getValorCofins());
       nfe.setValorCSLL(request.getValorCSLL());
       nfe.setValorIRRF(request.getValorIRRF());
       nfe.setValorFinal(request.getValorFinal());

       return nfe;
   }

}
