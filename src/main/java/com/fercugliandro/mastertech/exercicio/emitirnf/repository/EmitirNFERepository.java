package com.fercugliandro.mastertech.exercicio.emitirnf.repository;

import com.fercugliandro.mastertech.exercicio.emitirnf.model.EmissaoNFE;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EmitirNFERepository  extends JpaRepository<EmissaoNFE, Long> {

    Optional<List<EmissaoNFE>> findByIdentidade(String identidade);

}
