package com.fercugliandro.mastertech.exercicio.emitirnf.service.impl;

import com.fercugliandro.mastertech.exercicio.emitirnf.model.EmissaoNFE;
import com.fercugliandro.mastertech.exercicio.emitirnf.model.NFE;
import com.fercugliandro.mastertech.exercicio.emitirnf.model.RegistrarLog;
import com.fercugliandro.mastertech.exercicio.emitirnf.model.StatusNFE;
import com.fercugliandro.mastertech.exercicio.emitirnf.repository.EmitirNFERepository;
import com.fercugliandro.mastertech.exercicio.emitirnf.repository.NFeRepository;
import com.fercugliandro.mastertech.exercicio.emitirnf.service.EmitirNFEService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

@Service
public class EmitirNFEServiceImpl implements EmitirNFEService {

    @Autowired
    private EmitirNFERepository emitirNFERepository;

    @Autowired
    private NFeRepository nfeRepository;

    @Autowired
    private KafkaTemplate<String, EmissaoNFE> nfeSender;

    @Autowired
    private KafkaTemplate<String, RegistrarLog> logSender;


    @Override
    public EmissaoNFE emitirNFE(EmissaoNFE emissaoNfe) {

        EmissaoNFE emissaoNFE = emitirNFERepository.save(emissaoNfe);
        enviarNFE(emissaoNFE);
        registrarLog(emissaoNFE, "EMISSAO");

        return emissaoNFE;
    }

    @Override
    public NFE registrarValoresNFE(NFE nfe) {

        NFE nfeRetorno = nfeRepository.saveAndFlush(nfe);

        return nfeRetorno;
    }

    @Override
    public EmissaoNFE atualizarNFe(long id, long idNfe) {
        Optional<EmissaoNFE> optional = emitirNFERepository.findById(id);
        if (optional.isPresent()) {
            EmissaoNFE nfe = optional.get();
            nfe.setStatus(StatusNFE.COMPLETE);
            nfe.setNfe(new NFE());
            nfe.getNfe().setId(idNfe);
            nfe = emitirNFERepository.saveAndFlush(nfe);

            return nfe;
        }
        return null;
    }

    @Override
    public List<EmissaoNFE> obterNFeEmitidas(String identidade) {

        Optional optional = emitirNFERepository.findByIdentidade(identidade);
        if (!optional.isPresent()) {
            return null;
        }

        List<EmissaoNFE> nfes = (List<EmissaoNFE>) optional.get();
        registrarLog(nfes.get(0), "CONSULTA");

        return nfes;
    }

    private void enviarNFE(EmissaoNFE nfe) {
        nfeSender.send("luis-biro-0", "1", nfe);
    }

    private RegistrarLog obterLog(EmissaoNFE nfe, String tipo) {
        RegistrarLog log = new RegistrarLog();
        log.setIdentidade(nfe.getIdentidade());
        log.setTipo(tipo);
        log.setTimestamp(DateFormat.getInstance().format(Calendar.getInstance().getTimeInMillis()));
        if (tipo.equalsIgnoreCase("EMISSAO")) {
            log.setValor(nfe.getValor().toString());
        }


        return log;
    }

    private void registrarLog(EmissaoNFE nfe, String tipo) {
        RegistrarLog log = obterLog(nfe, tipo);
        logSender.send("luis-biro-1", "1", log);
    }

}
