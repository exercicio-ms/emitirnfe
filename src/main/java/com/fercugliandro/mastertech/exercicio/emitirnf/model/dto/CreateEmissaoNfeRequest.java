package com.fercugliandro.mastertech.exercicio.emitirnf.model.dto;

public class CreateEmissaoNfeRequest {

    private String identidade;
    private Double valor;

    public String getIdentidade() {
        return identidade;
    }

    public void setIdentidade(String identidade) {
        this.identidade = identidade;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
}
