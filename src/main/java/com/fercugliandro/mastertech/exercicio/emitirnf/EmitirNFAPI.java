package com.fercugliandro.mastertech.exercicio.emitirnf;

import com.fercugliandro.mastertech.exercicio.emitirnf.model.EmissaoNFE;
import com.fercugliandro.mastertech.exercicio.emitirnf.model.EmissaoNFEMapper;
import com.fercugliandro.mastertech.exercicio.emitirnf.model.NFE;
import com.fercugliandro.mastertech.exercicio.emitirnf.model.NFEMapper;
import com.fercugliandro.mastertech.exercicio.emitirnf.model.dto.CreateEmissaoNfeRequest;
import com.fercugliandro.mastertech.exercicio.emitirnf.model.dto.CreateEmissaoNfeResponse;
import com.fercugliandro.mastertech.exercicio.emitirnf.model.dto.CreateNFERequest;
import com.fercugliandro.mastertech.exercicio.emitirnf.service.EmitirNFEService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EmitirNFAPI {

    @Autowired
    private EmitirNFEService service;

    @Autowired
    private EmissaoNFEMapper emissaoNFEMapper;

    @Autowired
    private NFEMapper nfeMapper;

    @PostMapping("/nfe/emitir/")
    public ResponseEntity<?> emitirNF(@RequestBody CreateEmissaoNfeRequest request) {
        try {

            EmissaoNFE emissao = emissaoNFEMapper.toEmissaoNFE(request);
            emissao = service.emitirNFE(emissao);

            CreateEmissaoNfeResponse response = emissaoNFEMapper.toCreateEmissaoNFResponse(emissao);
            return new ResponseEntity<>(response, HttpStatus.CREATED);
        } catch (Exception e) {
            String errorMessage;
            errorMessage = e.getMessage() + " <== error";
            return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/nfe/gerarnfe/")
    public ResponseEntity<?> gerarNfe(@RequestBody CreateNFERequest requet) {
        try {
            NFE nfe = nfeMapper.toNFE(requet);
            nfe = service.registrarValoresNFE(nfe);

            service.atualizarNFe(requet.getIdEmissaoNFe(), nfe.getId());

            return new ResponseEntity<>(nfe, HttpStatus.CREATED);
        } catch (Exception e) {
            String errorMessage;
            errorMessage = e.getMessage() + " <== error";
            return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        }
    }


    @GetMapping("/nfe/consultar/{identidade}")
    public ResponseEntity<?> consultarNfe(@PathVariable String identidade) {
        try {
            List<EmissaoNFE> nfes = service.obterNFeEmitidas(identidade);
            return new ResponseEntity<>(nfes, HttpStatus.OK);
        } catch (Exception e) {
            String errorMessage;
            errorMessage = e.getMessage() + " <== error";
            return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        }
    }

}
