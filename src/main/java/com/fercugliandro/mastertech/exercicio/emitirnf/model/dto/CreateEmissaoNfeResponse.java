package com.fercugliandro.mastertech.exercicio.emitirnf.model.dto;

public class CreateEmissaoNfeResponse {

    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
