package com.fercugliandro.mastertech.exercicio.emitirnf.service;

import com.fercugliandro.mastertech.exercicio.emitirnf.model.EmissaoNFE;
import com.fercugliandro.mastertech.exercicio.emitirnf.model.NFE;

import java.util.List;

public interface EmitirNFEService {

    EmissaoNFE emitirNFE(EmissaoNFE emissaoNfe);

    NFE registrarValoresNFE(NFE nfe);

    EmissaoNFE atualizarNFe(long id, long idNfe);

    List<EmissaoNFE> obterNFeEmitidas(String identidade);

}
